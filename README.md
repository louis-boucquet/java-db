# java-db
The java-db is a simple database without `SQL`.  
The interface to the db is pure java which makes database stuff a lot easier.  
The models get saved in a very efficient and light weight way in files.

## Getting the DB

### Usage
#### Getting Tables
Instantiating a db is as simple as this:
```java
Table<Person> personTable = DataBase.get(Person.class);
```
Under the hood a model for `Person.class` gets made and from that a `Table` instance can be returned.  
If you do this again you will get the same instance again, to do this the `Database` class stores a `HashMap` which keeps all db's.

```java
Map<Class<? extends Identifiable>,
	Table<? extends Identifiable>> tables;
```

You see that the generic type of `Table` has to extend `Identifiable`, more on that in [Models](#Models)

#### Reading and Writing
Here's an example of writing an object and retrieving it again
```java
// add a person to the personTable
int id = personTable.add(person);

// retrieve it again with the id
Query<Person> query = personTable.get(id);

// complete the query
Person person = query.get();
```

`personTable.get(id)` doesn't return a `Person` object (what you might expect), instead it returns a `Query<Person>`, which is a wrapper around `Person` so you can ask to include fields.

#### Including Secondary links
It can happen that a `ModelObject` has a non-primitive field, this will have to be stored in a different table.
> Note: we only created a `Person` table but if needed the `Database` class will automatically create extra tables as needed

When retrieving such an object we don't "include" those none-primitive fields because this could execute extra reads that we don't need. Instead you can manually say which fields to include

```java
Person person = personTable
	.get(id)
	.includeField("pet")
	.get()
```

What if the included field `"pet"` itself has another non-primitive field?

```java
Person person = personTable
	.get(id)
	.includeField("pet", p -> p.includeField("toy"))
	.get()
```

From here on out you can nest as deeply as you want!

When you have a lot of fields this can get tedious if you need most of them, for that use

```java
Person person = personTable.getFull(id);
```
This will include all the fields for you.

### Configuration
You can expand by implementing your own `Table` by doing this.
```java
DataBase.setTableCreator((Class<? extends Identifiable> classType) -> {  
    return new CustomTable(classType);  
});
```
or simply
```java
DataBase.setTableCreator(CustomTable::new);
```

## Models
A `Table` can only store java objects that extends `Identifiable`, this is just an abstract class that keeps an integer id.

To let the Table know that it has to store a field, annotate is with `@DbField`

An example of Person model would be

```java
public class Person extends Identifiable {

    @DbField
    private int age;

    @DbField
    private String name;

    @DbField
    private Pet pet;
  
    public Person(int age, Pet pet) {
        // pass a standard id (this doesn't matter)
		super(0);
        this.age = age;
        this.pet = pet;
    }
}
```

> Note: `Pet` will be stored in a different table than `Person` so it also has to extend `Identifiable`
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE3OTg4Nzg1ODYsLTEzNTU2MDA0NDIsNz
MxNzc2NDk5LDE4NDU3NjAxNzYsNTU1MjU3MjU5XX0=
-->