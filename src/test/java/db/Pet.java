package db;

import db.model.DbField;
import db.model.Identifiable;

import java.util.Objects;

public class Pet extends Identifiable {

    @DbField
    private String name;

    @DbField
    private Toy toy;

    public Pet(int id, String name, Toy toy) {
        super(id);
        this.name = name;
        this.toy = toy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "name='" + name + '\'' +
                ", toy=" + toy +
                '}';
    }

    public Toy getToy() {
        return toy;
    }

    public void setToy(Toy toy) {
        this.toy = toy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return Objects.equals(name, pet.name) &&
                Objects.equals(toy, pet.toy);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, toy);
    }
}
