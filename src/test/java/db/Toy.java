package db;

import db.model.Identifiable;

import java.util.Objects;

public class Toy extends Identifiable {
    boolean bla;

    public boolean isBla() {
        return bla;
    }

    public void setBla(boolean bla) {
        this.bla = bla;
    }

    public Toy(int id, boolean bla) {
        super(id);
        this.bla = bla;
    }

    @Override
    public String toString() {
        return "Toy{" +
                "bla=" + bla +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Toy toy = (Toy) o;
        return bla == toy.bla;
    }

    @Override
    public int hashCode() {

        return Objects.hash(bla);
    }
}
