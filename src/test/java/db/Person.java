package db;

import db.model.DbField;
import db.model.Identifiable;

import java.util.Objects;

public class Person extends Identifiable {

    @DbField
    private int age;

    @DbField
    private Pet pet;

    public Person(int id, int age, db.Pet pet) {
        super(id);
        this.age = age;
        this.pet = pet;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + getId() +
                ", age=" + age +
                ", pet=" + pet +
                '}';
    }

    public Pet getPet() {
        return pet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age &&
                Objects.equals(pet, person.pet);
    }

    @Override
    public int hashCode() {

        return Objects.hash(age, pet);
    }
}
