package db.tables;

import db.Database;
import db.Person;
import db.Pet;
import db.Toy;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class TableTest {

    private Table<Person> personTable = Database.get(Person.class);

    private Toy toy = new Toy(0, true);
    private Pet pet = new Pet(0, "hond", toy);
    private Person person = new Person(0, 10, pet);

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void add() {
        int id = personTable.add(person);
        personTable.remove(id);
    }

    @Test
    public void get() {
        int id = personTable.add(person);

        Person person1 = personTable
                .get(id)
                .get();

        assertEquals(person1.getAge(), person.getAge());

        personTable.remove(id);
    }

    @Test
    public void getFull() {
        int id = personTable.add(person);

        Person person1 = personTable.getFull(id);

        assertEquals(person1, person);

        personTable.remove(id);
    }

    @Test
    public void update() {
        int id = personTable.add(person);

        Person person1 = personTable.getFull(id);

        person1.setAge(20);
        person1.getPet().setName("blablabla");
        person1.getPet().getToy().setBla(false);

        boolean success = personTable.update(person1);

        Person person2 = personTable.getFull(id);

        assertTrue(success);

        assertEquals(person1, person2);

        personTable.remove(id);
    }

    @Test
    public void remove() {
        int id = personTable.add(person);

        Person person = personTable.getFull(id);

        assertNotNull(person);

        personTable.remove(id);

        person = personTable.getFull(id);

        assertNull(person);
    }
}