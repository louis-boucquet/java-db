package db;

import db.tables.Table;

public class Main {
    public static void main(String[] args) {
        Table<Person> personTable = Database.get(Person.class);

        Pet pet = new Pet(0, "hond", new Toy(0, true));
        Person person = new Person(0, 10, pet);

        int id = personTable.add(person);

        person.setPet(null);
        pet.setToy(null);

        System.out.println("id: " + id);

        Person personQuery = personTable
                .get(id)
                .includeField("pet")
                .get();

        System.out.println(personQuery);
    }
}
