package db;

import db.model.Identifiable;
import db.tables.Table;
import db.tables.TablePOJ;

import java.util.HashMap;
import java.util.Map;

/**
 * This class contains (statically) all the Tables
 */
public class Database {
    private Database() {
    }

    private static TableCreator tableCreator = TablePOJ::new;

    private static final Map<Class<? extends Identifiable>, Table<? extends Identifiable>> tables = new HashMap<>();

    /**
     * If there ever was created a Table return is, otherwise make one
     *
     * @param classType the classType of the database
     * @return a Database instance
     */
    public static <T extends Identifiable> Table<T> get(Class<T> classType) {
        Table<T> table = (Table<T>) tables.get(classType);

        // return the existing table if it exists
        if (table != null)
            return table;

        // make a new table if one doesn't exist yet
        table = tableCreator.create(classType);
        tables.put(classType, table);

        return table;
    }

    /**
     * configure what type of table you want to use
     * @param tableCreator the new Table creator
     */
    public static void setTableCreator(TableCreator tableCreator) {
        Database.tableCreator = tableCreator;
    }

    private interface TableCreator {
        <T extends Identifiable> Table<T> create(Class<T> classType);
    }
}
