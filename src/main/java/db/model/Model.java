package db.model;

import db.Database;
import db.tables.Query;
import db.tables.Stored;
import db.tables.Table;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

public class Model<T extends Identifiable> {

    private Class<T> classType;

    // this maps fieldNames to their type
    private Map<String, Class<? extends Identifiable>> fields = new HashMap<>();

    public Model(Class<T> classType) {
        this.classType = classType;

        for (Field field : classType.getDeclaredFields()) {
            Class<?> type = field.getType();

            if (field.getAnnotation(DbField.class) != null && Identifiable.class.isAssignableFrom(type)) {

                fields.put(field.getName(), (Class<? extends Identifiable>) type);
            }
        }
    }

    /**
     * @return the fields of T
     */
    public Map<String, Class<? extends Identifiable>> getFields() {
        return fields;
    }

    /**
     * If you want to store a non-primitive fields you will need other tables.
     *
     * This method stores these fields and returns their id's
     *
     * @param t the object of which you want to store non primitive data
     * @return a map from fieldnames to their id's
     */
    public Map<String, Integer> storeSecondaryFields(T t) {
        Map<String, Integer> secondaryIDs = new HashMap<>();

        for (String fieldName : fields.keySet()) {
            Class<? extends Identifiable> type = fields.get(fieldName);
            Table<? extends Identifiable> table = Database.get(type);

            try {
                Field field = classType.getDeclaredField(fieldName);
                field.setAccessible(true);

                // get field value
                Object object = field.get(t);

                // store it in it's table
                int id = addField(table, object);

                // remember the id for later
                secondaryIDs.put(fieldName, id);

                // this should never be thrown
            } catch (IllegalAccessException | NoSuchFieldException ignored) {
            }
        }

        return secondaryIDs;
    }

    /**
     * This is a helper method because of generics
     */
    private <P extends Identifiable> int addField(Table<P> table, Object object) {
        P p = (P) object;
        return table.add(p);
    }

    /**
     * overload
     *
     * @param fieldName the field you want to include
     * @param stored the stored entity on which you want to execute the include
     */
    public void includeField(String fieldName, Stored<T> stored) {
        includeField(fieldName, q -> {
        }, stored);
    }

    /**
     * When you query an entity, it doesn't automatically include secondary fields.
     * To include these, call this function
     *
     * @param fieldName the field you want to include
     * @param includeFunction a that can execute includes on the included field
     * @param stored the stored entity on which you want to execute the include
     */
    public void includeField(String fieldName, Consumer<Query<?>> includeFunction, Stored<T> stored) {
        try {
            Field field = classType.getDeclaredField(fieldName);
            field.setAccessible(true);

            Class<? extends Identifiable> type = fields.get(fieldName);
            Table<? extends Identifiable> table = Database.get(type);

            // get the secondary id of the field
            int secondaryID = stored
                    .getSecondaryIDs()
                    .get(fieldName);

            // get Query
            Query<? extends Identifiable> query = table.get(secondaryID);

            // include sub fields of this field
            includeFunction.accept(query);

            // retrieve the field value from the table
            Identifiable identifiable = query.get();

            // include the value in the owner object
            field.set(stored.get(), identifiable);

            // when the field isn't found let it know
        } catch (IllegalAccessException e) {
            // this should never be thrown
        } catch (NoSuchFieldException e) {
            System.err.println("field " + fieldName + " is not registered on class " + classType);
        }
    }

    /**
     * Remove all non primitive fields in other tables
     *
     * @param remove This contains the entity that was removed and its secondary fields
     */
    public void remove(Stored<T> remove) {
        Map<String, Integer> secondaryIDs = remove.getSecondaryIDs();
        Set<String> fieldNames = secondaryIDs.keySet();

        for (String fieldName : fieldNames) {
            // get a table for this field
            Table<? extends Identifiable> table = Database.get(fields.get(fieldName));

            // get id of this field
            Integer id = secondaryIDs.get(fieldName);

            // remove it
            table.remove(id);
        }
    }

    /**
     * When updating, update everything in other tables as well
     *
     * @param stored what you want to update (entity and id's for other tables)
     */
    public void updateSecondaryFields(Stored<T> stored) {
        Map<String, Integer> secondaryIDs = stored.getSecondaryIDs();
        Set<String> fieldNames = secondaryIDs.keySet();

        for (String fieldName : fieldNames) {
            try {
                // get an accessible field
                Field field = classType.getDeclaredField(fieldName);
                field.setAccessible(true);

                // get a table for this field
                Table<? extends Identifiable> table = Database.get(fields.get(fieldName));

                // get id of this field
                Integer id = secondaryIDs.get(fieldName);

                // get field value
                Object fieldValue = field.get(stored.get());

                // use this helper method for generics
                updateInTable(table, id, fieldValue);
            } catch (NoSuchFieldException | IllegalAccessException ignored) {
            }
        }
    }

    /**
     * helper method for generics
     */
    private <P extends Identifiable> void updateInTable(Table<P> table, int id, Object o) {
        table.update(id, (P) o);
    }
}
