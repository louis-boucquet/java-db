package db.tables;

import db.model.Identifiable;

import java.sql.SQLException;

/**
 * @param <T>
 */
public interface Table<T extends Identifiable> {

    // GETTER CODE

    /**
     * @param id the id of the entity you want to get
     * @return the entity you requested
     */
    Query<T> get(int id) throws SQLException;

    /**
     * @param id the id of the entity you want to get
     * @return the entity you requested
     */
    T getFull(int id);

    // ADD CODE

    /**
     * @param t the entity you want to add
     * @return the id of the new entity
     */
    int add(T t);

    // REMOVE CODE

    /**
     * @param id the id of entity you want to remove
     * @return success
     */
    boolean remove(int id);

    /**
     * @param t remove this entity en get the id from it
     * @return success
     */
    default boolean remove(T t) {
        return remove(t.getId());
    }

    // UPDATE CODE

    /**
     * @param id id of the entity you want to update
     * @param t the new data of the entity
     * @return success
     */
    boolean update(int id, T t);

    /**
     * @param t the new data for the entity you want to update, the id of this entity is used
     * @return success
     */
    default boolean update(T t) {
        return update(t.getId(), t);
    }
}
