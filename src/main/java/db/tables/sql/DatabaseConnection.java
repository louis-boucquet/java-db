package db.tables.sql;

import java.sql.*;

public class DatabaseConnection implements AutoCloseable {

    private Connection conn;
    private PreparedStatement ps;

    public DatabaseConnection(String driver) {
        try {
            this.conn = DriverManager.getConnection(driver);
            conn.setAutoCommit(false);
        } catch (SQLException e) {
            System.err.println("Failed to connect to database.");
            e.printStackTrace();
        }
    }

    /**
     * Update the database
     */
    public boolean updateDatabase() {
        try {
            conn.commit();
            return true;
        } catch (SQLException e) {
            System.err.println("Failed updating database.");
            e.printStackTrace();
        }
        return false;
    }


    /**
     * Update the database
     */
    public boolean updateDatabase(String query, Object[] params) {
        addBatch(query, params);
        return updateDatabase();
    }

    /**
     * Adds a addBatch statement to a preparedStatement
     */
    public void addBatch(String query, Object[] params) {
        try {
            this.ps = conn.prepareStatement(query);
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    this.ps.setString(i + 1, params[i].toString());
                }
            }
            ps.execute();
        } catch (SQLException e) {
            System.err.println("Failed to create PreparedStatement.");
            e.printStackTrace();
        }
    }

    /**
     * Executes the full addBatch with all the batches already done
     *
     * @param query if you want to add a last addBatch
     * @return The resultset of the query
     */
    public ResultSet executeQuery(String query, String[] param) {
        addBatch(query, param);
        try {
            return ps.executeQuery();
        } catch (SQLException e) {
            System.err.println("Failed to execute addBatch.");
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void close() throws Exception {
        this.conn.close();
    }
}
