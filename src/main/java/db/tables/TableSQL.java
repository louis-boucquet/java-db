package db.tables;

import db.model.Identifiable;
import db.tables.sql.DatabaseConnection;

import java.sql.SQLException;

public class TableSQL<T extends Identifiable> implements Table<T> {

    private final DatabaseConnection conn;
    private final String name;

    public TableSQL(DatabaseConnection conn, String name) throws SQLException {
        this.conn = conn;
        this.name = name;
    }

    @Override
    public Query<T> get(int id) throws SQLException {
        conn.addBatch("SELECT * FROM ? WHERE id=?", new Object[]{getName(), id});

        return null;
    }

    private String getName() {
        return this.name;
    }

    @Override
    public T getFull(int id) {
        return null;
    }

    @Override
    public int add(T t) {
        return 0;
    }

    @Override
    public boolean remove(int id) {
        return false;
    }

    @Override
    public boolean update(int id, T t) {
        return false;
    }
}
