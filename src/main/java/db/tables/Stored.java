package db.tables;

import db.model.Identifiable;

import java.util.Map;

public class Stored<T extends Identifiable> {

    private final T t;
    private final Map<String, Integer> secondaryIDs;

    public Stored(T t, Map<String, Integer> fields) {
        this.t = t;
        this.secondaryIDs = fields;
    }

    public T get() {
        return t;
    }

    public Map<String, Integer> getSecondaryIDs() {
        return secondaryIDs;
    }
}
