package db.tables;

import db.model.Identifiable;
import db.model.Model;

import java.util.function.Consumer;

public class Query<T extends Identifiable> {

    private final Stored<T> stored;
    private final Model<T> model;

    public Query(Stored<T> stored, Model<T> model) {
        this.stored = stored;
        this.model = model;
    }

    public Query<T> includeField(String fieldName) {
        model.includeField(fieldName, stored);

        return this;
    }

    public Query<T> includeField(String fieldName, Consumer<Query<?>> includeFunction) {
        model.includeField(fieldName, includeFunction, stored);

        return this;
    }

    public T get() {
        if (stored == null)
            return null;

        return stored.get();
    }

    public Query<T> includeAll() {
        if (stored == null)
            return this;

        for (String fieldName : stored.getSecondaryIDs().keySet())
            includeField(fieldName, Query::includeAll);
        return this;
    }
}
