package db.tables;

import db.model.Identifiable;
import db.model.Model;

import java.util.HashMap;
import java.util.Map;

public class TablePOJ<T extends Identifiable> implements Table<T> {

    private Map<Integer, Stored<T>> entities = new HashMap<>();

    // counter for id's
    private int index = 0;

    private Model<T> model;

    public TablePOJ(Class<T> classType) {
        System.out.println("creating table " + classType);
        this.model = new Model<>(classType);
    }

    @Override
    public Query<T> get(int id) {
        Stored<T> stored = entities.get(id);

        // set the correct id
        if (stored != null)
            stored.get()
                    .setId(id);

        return new Query<>(stored, model);
    }

    @Override
    public T getFull(int id) {
        return get(id)
                .includeAll()
                .get();
    }

    @Override
    public int add(T t) {
        // t can't be null
        if (t == null)
            throw new NullPointerException();

        // store secondary fields in other tables
        Map<String, Integer> secondaryIDs = model.storeSecondaryFields(t);

        // remember secondary keys
        entities.put(index, new Stored<>(t, secondaryIDs));

        return index++;
    }

    @Override
    public boolean remove(int id) {
        Stored<T> remove = entities.remove(id);

        model.remove(remove);

        // return true if an entity was removed
        return remove != null;
    }

    @Override
    public boolean update(int id, T t) {
        // t can't be null
        if (t == null)
            throw new NullPointerException();

        Stored<T> stored = entities.get(id);

        // check if an entity already exists
        if (stored == null)
            return false;

        // update stored
        stored = new Stored<>(t, stored.getSecondaryIDs());

        model.updateSecondaryFields(stored);

        entities.put(id, stored);

        return true;
    }
}
